<?php

namespace SDA\Blueservices\Controller;

use Sda\Blueservices\DataRSS;


class Controller {
    
    private $template;
    private $params = [];

    public function __construct(
            $template
            ){
        $this->template = $template;
    }
    
    public function run() {
        
        if (array_key_exists('action', $_GET)) {
            $_POST['action'] = 'dataRSS';

            $this->getURL();
            $this->template->renderTemplate('mainPage.html', $this->params);
        }  
    }
    
    public function getURL(){
        
        if (array_key_exists('dataRSS', $_POST)) {
          $dataURL = $_POST['dataRSS']; 
        }
        
        if (filter_var($dataURL, FILTER_VALIDATE_URL)) {
            $url = new DataRSS(htmlspecialchars($dataURL));
            $informationRSS = $url->getRSS($dataURL);
            $this->params['RSSnumber'] = $informationRSS;
            $this->params['RSS'] = $informationRSS['entry'];
          
        } else {
            echo("$dataURL jest niepoprawnym adresem URL");
        }
    }
}
