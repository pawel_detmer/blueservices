<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Sda\Blueservices\Controller\Controller;
use Sda\Blueservices\Template\Template;

$loader = new Twig_Loader_Filesystem(__DIR__ . '/Template/templates');
$twig = new Twig_Environment($loader,['cache' => false]);
$template = new Template($twig);

$app = new Controller (
        $template
        );

$app->run();