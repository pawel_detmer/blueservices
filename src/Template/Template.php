<?php

namespace Sda\Blueservices\Template;

use Twig_Environment;

/**
 * Class Template
 * @package Sda\Project\Template
 */
class Template
{


    /**
     * @var Twig_Environment
     */
    private $twig;

    /**
     * Template constructor.
     * @param Twig_Environment $twig
     */
    public function __construct(Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @param string $templateName
     * @param array $parameters
     */
    public function renderTemplate($templateName, array $parameters)
    {
        /** @noinspection ExceptionsAnnotatingAndHandlingInspection */
        echo $this->twig->render($templateName, $parameters);
    }

}
