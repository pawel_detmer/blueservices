<?php

namespace Sda\Blueservices;

class NewSpecyficDataRSS {
    
    private $id;
    private $title;
    private $published;
    private $updated;
    
    
    public function __construct($id, $title, $published, $updated) {
        $this->id = $id;
        $this->title = $title;
        $this->published = $published;
        $this->updated = $updated;
    }
    
    public function getId() {
        return $this->id;
    }
    
    public function getTitle() {
        return $this->title;
    }
    
    public function getPublished() {
        return $this->published;
    }
    
    public function getUpdated() {
        return $this->updated;
    }
    
}
