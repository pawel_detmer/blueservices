<?php 
namespace SDA\Blueservices;

class DataRSS {
    
    private $url;
    
    public function __construct(
            $url 
            ) {
        $this->url = $url;
    }
   
    public function getURL() {
        return $this->url;
    }
    
    public function getRSS($url) {
        $xml = simplexml_load_file($url);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);
        return $array;
    }

}
