<?php

namespace Sda\Blueservices;

use Doctrine\DBAL\Connection;

class DataRSSRepository {
      /**
     * @var Connection
     */
    private $dbh;
   

    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }
    
    public function addNewRSS($newSpecyficDataRSS)
    {
        $this->dbh->insert('blueservices', [
            'title' => $newSpecyficDataRSS->getId(),
            'id' => $newSpecyficDataRSS->getTitle(),
            'ublished' => $newSpecyficDataRSS->getPublished(),
            'updated' => $newSpecyficDataRSS->getUpdated(),
        ]);
    }
    
    
}
